﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Haszowanie
{
    class HashList <T> where T : IComparable
    {
        private uint size;
        private int count = 0;

        public double Count
        { get { return count; } }


        private element[] list;

        //delegat funkcji haszujacej
        public delegate uint dHash(T val, uint size);
        public dHash Hash;


        #region KONSTRUKTOR
        public HashList(uint size)
        {
            this.size = size;

            Hash = HashFun.Default;

            list = new element[size];

            for (int i = 0; i < size; i++)
                list[i] = new element();
        }

        public HashList(uint size, dHash HashFun)
        {
            this.size = size;

            Hash = HashFun;

            list = new element[size];

            for (int i = 0; i < size; i++)
                list[i] = new element();
        }
        #endregion

       
        private void ResizeList(uint new_size)
        {
            List<T> TMP_list = new List<T>();


            //kopiowanie wszystkich wartosci do tymczasowej list
            foreach (var l in list)
                if(l.empety != true) TMP_list.Add(l.value);

            //powiekszanie tablicy
            size = new_size;

            list = new element[size];

            for (int i = 0; i < size; i++)
                list[i] = new element();

            count = 0;

            foreach (var l in TMP_list)
                Add(l);

            TMP_list.Clear();

        }
    

        #region funkcja dodajaca wartosc do listy, zwraca index
        public int Add(T val)
        {
            int idx;

            idx = (int)Hash(val, size);


            if ((Mem(val)==-1)){
                while (!list[idx].empety)
                {
                    idx = (idx + 1) % (int)size;
                }          
     
                list[idx].value = val;
                list[idx].empety = false;

                count++;
            }


            if (count >= size)
                ResizeList( 2 * size);

            return idx;
        }

        public int Del(T val)
        {
            int idx;

            idx = Mem(val);

            //usuwanie wartosci, gdy znajduje sie w liscie
            if (idx != -1)
            {
                list[idx].delete();
                count--;
            }

            if (count <= (size/2))
                ResizeList((uint)(size/2));

            return idx;
        }

        public int Mem(T val)
        {
            int idx = (int)Hash(val, size);
            bool found = false;

            while (!found)
            {

                if ((list[idx].empety == true))
                    return -1;

                found = Compare(list[(idx)].value, val);

                if(!found)
                    idx = (idx + 1) % (int)size;
            }

            return idx;
        }

        public bool Mem(T val, int idx)
        {
            return Compare(list[idx].value, val);
        }
        #endregion

        #region SHOW
        public string ShowIdx(int idx)
        {
            if (idx >= size)
                throw new IndexOutOfRangeException("Wartosc poza lista");

            string str = "";

            if(!list[idx].empety) str = list[idx].value.ToString();

            return str;
        }



        public void Show()
        {
            Show(0, (int)(size - 1));
        }

        public void Show(int to)
        {
            Show(0, to);
        }

        public void Show(int from, int to)
        {
            if (from < 0 || from >= size || from > to || to >= size)
                throw new IndexOutOfRangeException();

            for (int i = from; i <= to; i++)
            {
                
                if (list[i].empety != true)
                {
                    Console.WriteLine("{0}.\t{1}", i + 1, ShowIdx(i));
                }
                else
                    WriteGreen(((i + 1) + "."));
            }

        }

        /*
        public void Stat()
        {
            int[] cop = new int[count + 1];

            for (int i = 0; i < count + 1; i++)
                cop[i] = 0;

            foreach(var l in list)
            {
                cop[l.Count] += 1;
            }

            Console.WriteLine("*--------------------------------------------------*");
            Console.WriteLine("Liczba elementów: {0}\nWielkosc tablicy: {1}", count, size);

            Console.WriteLine("liczba elementów na pozycje");
            for (int i = 0; i < 10; i++)
                if(cop[i] > 0) Console.WriteLine(i + "   " + cop[i]);

            Console.WriteLine("*--------------------------------------------------*");

        }
        */

        //misc
        static void WriteGreen(string value)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine(value);

            Console.ResetColor();
        }

        private bool Compare(T x, T y)
        {
            return EqualityComparer<T>.Default.Equals(x, y);
        }
        #endregion

        private class element
        {
            public T value = default(T);

            public bool empety = true;

            public void delete()
            {
                value = default(T);
                empety = true;
            }
        }

        public static class HashFun
        {
            public static uint Default(T val, uint size)
            {
                byte[] binary = getBinary(val);

                int i;
                uint h = 0;

                for (i = 0; i < binary.Length; i++)
                    h += binary[i];

                return (uint)h % size;
            }

            public static uint Djb2(T val, uint size)
            {
                byte[] binary = getBinary(val);
                int i;

                ulong h = 5381;

                for (i = 0; i < binary.Length; i++)
                {
                    var c = binary[i];
                    h = ((h << 5) + h) + c;
                }

                return (uint)h % size;
            }

            public static uint Sdbm(T val, uint size)
            {
                byte[] binary = getBinary(val);
                int i;

                ulong h = 0;

                h = 0;

                for (i = 0; i < binary.Length; i++)
                {
                    var c = binary[i];
                    h = c + (h << 6) + (h << 16) - h;
                }

                return (uint)h % size;
            }

            public static uint Multi31(T val, uint size)
            {
                byte[] binary = getBinary(val);

                uint h = 7;
                for (int i = 0; i < binary.Length; i++)
                {
                    var c = binary[i];
                    h = h * 31 + c;
                }

                return h % size;
            }


            
            private static byte[] getBinary(T val)
            {
                BinaryFormatter bf = new BinaryFormatter();

                using (MemoryStream ms = new MemoryStream())
                {
                    bf.Serialize(ms, val);
                    return ms.ToArray();
                }
            }
        }

    }
}
