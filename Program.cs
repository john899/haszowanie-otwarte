﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Haszowanie
{
    class Program
    {
        static List<string> GetWordDictionary()
        {
            List<string> words = new List<string>();

            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\words.txt";

            try
            {
                using (StreamReader sr = File.OpenText(path))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        words.Add(s);
                    }
                }
            }
            catch (UnauthorizedAccessException UAEx)
            {
                Console.WriteLine(UAEx.Message);
            }
            catch (PathTooLongException PathEx)
            {
                Console.WriteLine(PathEx.Message);
            }
            catch(System.IO.DirectoryNotFoundException DNFEx)
            {
                Console.WriteLine(DNFEx.Message);
                return null;
            }

            return words;

        }

        static List<string> GetSentenceDictionary()
        {
            List<string> sent = new List<string>();

            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\sent.txt";

            try
            {
                using (StreamReader sr = File.OpenText(path))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        string se = "";
                        foreach (var w in s)
                            se += w + " ";

                        sent.Add(s);
                    }
                }
            }
            catch (UnauthorizedAccessException UAEx)
            {
                Console.WriteLine(UAEx.Message);
            }
            catch (PathTooLongException PathEx)
            {
                Console.WriteLine(PathEx.Message);
            }
            catch (System.IO.DirectoryNotFoundException DNFEx)
            {
                Console.WriteLine(DNFEx.Message);
                return null;
            }

            return sent;

        }

        static List<int> GetRandomNumberDictionary(){

            List<int> num = new List<int>();

            var rnd = new Random();

            for(int i = 0; i < 100; i++)
                num.Add((rnd.Next(1000, 9999)));

            return num;  
        }


        static void Main(string[] args)
        {

            HashList<int>.dHash hashfun = HashList<int>.HashFun.Default;
            List<int> words = GetRandomNumberDictionary();

            HashList<int> hl = new HashList<int> (2, hashfun);



            for(int i = 0; i<words.Count; i++)
            {           
                var input = words[i];
                hl.Add(input);              
            }


            hl.Show();

            //hl.Stat();

            Console.ReadKey();
        }
    }
}
